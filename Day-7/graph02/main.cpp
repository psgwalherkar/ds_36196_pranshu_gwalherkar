/*****************************************************************
* File: main.cpp
* Batch: DAC Feb 2020 @ Sunbeam Infotech
* Created on: 25-Dec-2020
* Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
******************************************************************/
#include <iostream>
using namespace std;

#define INF 999
#define MAX 10

class AdjMatWeightedGraph {
private:
	int mat[MAX][MAX];
	int vertCount, edgeCount;
public:
	AdjMatWeightedGraph(int vertexCount) {
		int i, j;
		edgeCount = 0;
		vertCount = vertexCount;
		for(i=0; i<vertCount; i++) {
			for(j=0; j<vertCount; j++)
				mat[i][j] = INF;
		}
	}
	void accept() {
		cout << "enter number of edges: ";
		cin >> edgeCount;
		for(int c=0; c<edgeCount; c++) {
			int from, to, weight;
			cout << "enter edge (from to): ";
			cin >> from >> to >> weight;
			mat[from][to] = weight;
			mat[to][from] = weight; // comment this line for Directed graph.
		}
	}
	void display() {
		cout << "Adjacency Matrix: " << endl;
		for (int i = 0; i < vertCount; ++i) {
			for (int j = 0; j < vertCount; ++j) {
				if(mat[i][j] != INF)
					cout << mat[i][j] << "\t";
				else
					cout << "#" << "\t";
			}
			cout << endl;
		}
	}
};

int main() {
	int vertCount;
	cout << "enter number of vertices: ";
	cin >> vertCount;

	AdjMatWeightedGraph g(vertCount);
	g.accept();
	g.display();
	return 0;
}

/*
6
7
0 1 7
0 2 4
0 3 8
1 2 9
1 4 5
3 4 6
3 5 2
*/

























