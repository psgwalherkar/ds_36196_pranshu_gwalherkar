/*****************************************************************
* File: main.cpp
* Batch: DAC Feb 2020 @ Sunbeam Infotech
* Created on: 25-Dec-2020
* Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
******************************************************************/
#include <iostream>
using namespace std;

#define MAX 10

class AdjMatNonWeightedGraph {
private:
	int mat[MAX][MAX];
	int vertCount, edgeCount;
public:
	AdjMatNonWeightedGraph(int vertexCount) {
		int i, j;
		edgeCount = 0;
		vertCount = vertexCount;
		for(i=0; i<vertCount; i++) {
			for(j=0; j<vertCount; j++)
				mat[i][j] = 0;
		}
	}
	void accept() {
		cout << "enter number of edges: ";
		cin >> edgeCount;
		for(int c=0; c<edgeCount; c++) {
			int from, to;
			cout << "enter edge (from to): ";
			cin >> from >> to;
			mat[from][to] = 1;
			mat[to][from] = 1; // comment this line for Directed graph.
		}
	}
	void display() {
		cout << "Adjacency Matrix: " << endl;
		for (int i = 0; i < vertCount; ++i) {
			for (int j = 0; j < vertCount; ++j)
				cout << mat[i][j] << "\t";
			cout << endl;
		}
	}
};

int main() {
	int vertCount;
	cout << "enter number of vertices: ";
	cin >> vertCount;

	AdjMatNonWeightedGraph g(vertCount);
	g.accept();
	g.display();
	return 0;
}

/*
6
7
0 1
0 2
0 3
1 2
1 4
3 4
3 5
*/
























