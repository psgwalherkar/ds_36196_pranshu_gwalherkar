#include<iostream>
#include<vector>
using namespace std;
int main( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);
	v.push_back(40);
	v.push_back(50);

	vector<int>::iterator itrStart =  v.begin();
	v.erase( itrStart + 2 );
	for( int index = 0; index < v.size(); ++ index )
	{
		int element = v[ index ];
		cout<<element<<endl;
	}
	return 0;
}
int main5( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);

	for( int element : v )
		cout<<element<<endl;
	return 0;
}
int main4( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);

	vector<int>::iterator itrStart =  v.begin();
	vector<int>::iterator itrEnd =  v.end();
	while( itrStart != itrEnd )
	{
		int element = *( itrStart );
		cout<<element<<endl;
		++ itrStart;
	}
	return 0;
}
int main3( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);
	for( int index = 0; index < v.size(); ++ index )
	{
		int element = v[ index ];	//element = v.operator [](index);
		cout<<element<<endl;
	}
	return 0;
}
int main2( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);
	for( int index = 0; index < v.size(); ++ index )
	{
		int element = v.at( index );
		cout<<element<<endl;
	}
	return 0;
}
int main1( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(20);
	v.push_back(30);

	cout<<"Count	:	"<<v.size()<<endl;
	return 0;
}
