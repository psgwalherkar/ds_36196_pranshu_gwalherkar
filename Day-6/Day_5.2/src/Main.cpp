#include<iostream>
using namespace std;
#define SIZE	5
int get_hash_code( int data )
{
	const int PRIME = 151;
	int result = 1;
	result = result * data + PRIME;
	return result;
}
int main( void )
{
	for( int count = 1; count <= 200; ++ count )
	{
		int data = count;
		int hashCode = ::get_hash_code(data);
		int slot = hashCode % SIZE ;
		cout<<data<<"	"<<hashCode<<"	"<<slot<<endl;
	}
	return 0;
}
