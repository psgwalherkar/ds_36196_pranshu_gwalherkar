#include<iostream>
#include<string>
using namespace std;

class ArrayIndexOutOfBoundsException
{
private:
	string message;
public:
	ArrayIndexOutOfBoundsException( string message ) : message( message )
	{	}
	string getMessage( void )const
	{
		return this->message;
	}
};
class Array
{
private:
	int size;
	int *arr;
public:
	Array( void ) : size( 0 ), arr( NULL )
	{	}
	Array( int size ) throw( bad_alloc )
	{
		this->size = size;
		this->arr = new int[ this->size ];
	}
	~Array( void )
	{
		if( this->arr != NULL )
		{
			delete[] this->arr;
			this->arr = NULL;
		}
	}
	/*int find( int key )
	{
		for( int index = 0; index < this->size; ++ index )
		{
			if( arr[ index ] == key )
				return index;
		}
		return -1;
	}*/
	int find( int key )
	{
		int left = 0;
		int right = this->size - 1;
		while( left <= right )
		{
			int mid = ( left + right ) / 2;
			if( key == arr[ mid ] )
				return mid;
			else if ( key < arr[ mid ] )
				right = mid - 1;
			else
				left = mid + 1;
		}
		return -1;
	}
	int operator[ ]( int index )throw( ArrayIndexOutOfBoundsException )
	{
		if( index >= 0 && index < this->size )
			return this->arr[ index ];
		throw ArrayIndexOutOfBoundsException("Array Index Out Of Bounds Exception");
	}
	friend istream& operator>>( istream &cin, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
		{
			cout<<"arr[ "<<index <<" ]	:	";
			cin>>other.arr[ index ];
		}
		return cin;
	}
	friend ostream& operator<<( ostream &cout, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
			cout<<"arr[ "<<index <<" ]	:	"<<other.arr[ index ]<<endl;
		cout<<endl;
		return cout;
	}
};

int main( void )
{
	Array a1( 5 );
	cin >> a1;	//operator>>( cin, a1 )
	cout<<endl;
	cout << a1;	//operator<<( cout, a1 )

	int key = 50;
	int index = a1.find( key );
	if( index != -1 )
		cout<<"Key found at index : "<<index<<endl;
	else
		cout<<"Key not found"<<endl;
	return 0;
}
