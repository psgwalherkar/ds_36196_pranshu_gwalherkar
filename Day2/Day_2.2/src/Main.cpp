#include<iostream>
#include<list>
using namespace std;

int main( void )
{
	list<int> intList;
	intList.push_front(10);
	intList.push_back(20);
	intList.push_back(30);
	intList.push_back(40);
	intList.push_back(50);

	intList.pop_front();	//10
	intList.pop_back();		//50

	for( int element : intList )
		cout<<element<<"	";
	cout<<endl;
	return 0;
}
int main1( void )
{
	list<int> intList;
	intList.push_front(10);
	intList.push_back(20);
	intList.push_back(30);
	intList.push_back(40);
	intList.push_back(50);

	list<int>::iterator itrStart = intList.begin();
	list<int>::iterator itrEnd = intList.end();
	while( itrStart != itrEnd )
	{
		cout<<( *itrStart )<<"	";
		++ itrStart;
	}
	cout<<endl;
	return 0;
}
