#include<iostream>
#include<string>
using namespace std;
namespace collection
{
	class Exception
	{
	private:
		string message;
	public:
		Exception( string message = "" ) throw( ) : message( message )
		{	}
		string getMessage( )const throw( )
		{
			return this->message;
		}
	};
	class LinkedList
	{
	private:
		class Node
		{
		private:
			int data;
			Node *next;
		public:
			Node( int data ) : data( data ), next( NULL )
			{	}
			friend class LinkedList;
		};
	private:
		Node *head;
		Node *tail;
	public:
		LinkedList( void ) throw( ) : head( NULL ), tail( NULL )
		{	}
		bool empty( void )const throw( )
		{
			return this->head == NULL;
		}
		void addFirst( int data )throw( bad_alloc )
		{
			Node *newNode = new Node( data );
			if( this->empty( ) )
				this->tail = newNode;
			else
				newNode->next = this->head;
			this->head = newNode;
		}
		int getFirst( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception("LinkedList is empty");
			return this->head->data;
		}
		void removeFirst( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception("LinkedList is empty");
			else if( this->head == this->tail )
			{
				delete this->head;
				this->head = this->tail = NULL;
			}
			else
			{
				Node *trav = this->head;
				this->head = this->head->next;
				delete trav;
			}
		}
		~LinkedList( void )throw( )
		{
			while( !this->empty( ) )
				this->removeFirst();
		}
	};
	class StackUnderflowException
	{
	private:
		string message;
	public:
		StackUnderflowException( string message = "" ) throw( ) : message( message )
		{	}
		string getMessage( )const throw( )
		{
			return this->message;
		}
	};
	class Stack
	{
	private:
		LinkedList list;
	public:
		Stack( void )throw( )
		{	}
		bool empty( void )const throw( )
		{
			return this->list.empty();
		}
		void push( int element )throw( bad_alloc )
		{
			this->list.addFirst(element);
		}
		int peek( void )throw( StackUnderflowException )
		{
			if(this->empty( ) )
				throw StackUnderflowException("Stack is empty");
			return this->list.getFirst();
		}
		void pop( void )throw( StackUnderflowException )
		{
			if(this->empty( ) )
				throw StackUnderflowException("Stack is empty");
			this->list.removeFirst();
		}
		~Stack( void )
		{
			while( !this->empty( ) )
				this->pop( );
		}
	};
}
void accept_record( int &element )
{
	cout<<"Enter element	:	";
	cin>>element;
}
void print_record( int &element )
{
	cout<<"Popped element is	:	"<<element<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Push."<<endl;
	cout<<"2.Pop."<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice, element;
	using namespace collection;
	Stack stk;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			switch( choice )
			{
			case 1:
				::accept_record(element);
				stk.push( element );
				break;
			case 2:
				element = stk.peek( );
				::print_record(element);
				stk.pop( );
				break;
			}
		}
		catch( StackUnderflowException &ex )
		{
			cout<<ex.getMessage()<<endl;
		}
	}
	return 0;
}
