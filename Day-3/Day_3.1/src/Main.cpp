#include<iostream>
#include<string>
using namespace std;
namespace collection
{
	class Exception
	{
	private:
		string message;
	public:
		Exception( string message ) throw( ) : message( message )
		{	}
		string getMessage( void )const throw( )
		{
			return this->message;
		}
	};
	#define size 5
	class Stack
	{
	private:
		int top;
		int arr[ size ];
	public:
		Stack( void ) throw( ) : top( -1 )
		{	}
		bool empty( void )const throw( )
		{
			return this->top == -1;
		}
		bool full( void )const throw( )
		{
			return this->top == size - 1;
		}
		void push( int element )throw( Exception )
		{
			if( this->full( ) )
				throw Exception("Stack is full");
			this->arr[ ++ this->top ] = element;
		}
		int peek( void )const throw( Exception )
		{
			if( this->empty( ) )
				throw Exception("Stack is empty");
			return this->arr[ this->top ];
		}
		void pop( void )
		{
			if( this->empty( ) )
				throw Exception("Stack is empty");
			-- this->top;
		}
	};
}
void accept_record( int &element )
{
	cout<<"Enter element	:	";
	cin>>element;
}
void print_record( const int &element )
{
	cout<<"Popped element is	:	"<<element<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Push"<<endl;
	cout<<"2.Pop"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice, element;
	using namespace collection;
	Stack stk;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			switch( choice )
			{
			case 1:
				::accept_record(element);
				stk.push( element );
				break;
			case 2:
				element = stk.peek( );
				::print_record(element);
				stk.pop( );
				break;
			}
		}
		catch( Exception &ex )
		{
			cout<<ex.getMessage( )<<endl;
		}
	}
	return 0;
}
