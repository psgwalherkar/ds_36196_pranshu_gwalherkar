#include<iostream>
#include<string>
using namespace std;
namespace collection
{
	class Exception
	{
	private:
		string message;
	public:
		Exception( string message = "" ) throw( ) : message( message )
		{	}
		string getMessage( )const throw( )
		{
			return this->message;
		}
	};
	class Stack
	{
	private:
		int top;
		int size;
		int **arr;
	public:
		Stack( void ) throw( bad_alloc ) : top( -1 ), size( 5 )
		{
			this->arr = new int*[ this->size ];
		}
		Stack( int size )throw( bad_alloc ) : top( -1 ), size( size )
		{
			this->arr = new int*[ this->size ];
		}
		bool empty( void )const throw( )
		{
			return this->top == -1;
		}
		bool full( void )const throw( )
		{
			return this->top == this->size - 1;
		}
		void push( int element )throw( bad_alloc, Exception )
		{
			if( this->full( ) )
				throw Exception("Stack is full");
			this->arr[ ++ this->top ] = new int( element );
		}
		int peek( void )throw( Exception )
		{
			if(this->empty( ) )
				throw Exception("Stack is empty");
			return *( this->arr[ this->top ] );
		}
		void pop( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception("Stack is empty");
			delete this->arr[ this->top ];
			this->arr[ this->top ] = NULL;
			-- this->top;
		}
		~Stack( void )
		{
			while( !this->empty( ) )
				this->pop( );
			delete[] this->arr;
			this->arr = NULL;
		}
	};
}
void accept_record( int &element )
{
	cout<<"Enter element	:	";
	cin>>element;
}
void print_record( int &element )
{
	cout<<"Popped element is	:	"<<element<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Push."<<endl;
	cout<<"2.Pop."<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice, element;
	using namespace collection;
	Stack stk( 5 );
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			switch( choice )
			{
			case 1:
				::accept_record(element);
				stk.push( element );
				break;
			case 2:
				element = stk.peek( );
				::print_record(element);
				stk.pop( );
				break;
			}
		}
		catch( Exception &ex )
		{
			cout<<ex.getMessage()<<endl;
		}
	}
	return 0;
}
